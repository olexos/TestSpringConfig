package com.usb.spr.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Oleksiy on 12.02.2017.
 */
@Controller
public class HomeController {

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model uiModel) {
        return "home";
    }

    @RequestMapping(value = "/dictionary", method = RequestMethod.GET)
    public String dictionary(@RequestParam(value = "dictname", required = false)String dictName,
                             Model uiModel) {
        uiModel.addAttribute("dictName", dictName);
        System.out.println("!!!!! dicctionary name " + dictName );
        return "dict";
    }

  //  @Secured("ROLE_FRCPM")
    @RequestMapping(value = "/secured", method = RequestMethod.GET)
    @PreAuthorize("hasRole('FRCPM')")
    public String secur(Model uiModel) {
        return "secured";
    }


    @RequestMapping(value = "/preAuthorized", method = RequestMethod.GET)
    @PreAuthorize("hasRole('FRCADMIN')")
    public String preAuthorized(Model uiModel) {
        return "preauthorized";
    }


}
